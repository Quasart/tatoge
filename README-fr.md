TaToGE permet de simuler les divers accessoires nécessaires pour jouer aux jeux de sociétés, de cartes ou aux jeux de rôles (Dés, sabliers, compteurs de score...), regroupés ensemble sur un écran clair et épuré.

Ce logiciel se veut simple et générique, et destiné à être utilisé pour accompagner une "vraie" partie sur table.

Accessible ici : [TaToGE sur framagit pages](https://quasart.frama.io/tatoge/).

Le code source est disponible sous la licence [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).

## Utilisation typique

- Quand on a un matériel de jeu perdu ou manquant, ou si on veut reconstituer un jeu qu'on n'a pas.
- Quand lancer de vrais dés serait trop bruyant.
- Quand on a besoin d'un tirage aléatoire personalisé, par exemple pour choisir celui qui paye la tournée.
- Quand le maître du jeu veut un tableau de bord propre et paramétrable.
- Quand on veut diffuser un score sur un grand écran.

## Features

- [x] Sablier
- [x] Compteur                      
- [x] Dé                         
- [ ] Tirage de cartes
- [x] Écran "responsive"
- [x] Chargement depuis Json
- [ ] Export en json
- [ ] Internationalisation

