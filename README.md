TaToGE allows you to emulate the common game equipment needed when playing board games or card games (Dice, sandtimers, score counters...), and to display them all together in a clean screen.

It focuses on simplicity, and on the idea it will be used as a companion during "real" table-top gaming.

Available here: [TaToGE on framagit pages](https://quasart.frama.io/tatoge/).

Source code is released under [GNU GPL v3.0](https://www.gnu.org/licenses/gpl-3.0.en.html) license.

## Typical use

- When you have equipment lost or missing, or you want to simulate a game you don't have.
- When rolling real dice would be too noisy.
- When you need custom random generator, for example to choose who will pay the beers.
- When role-playing game master wants a clean customizable dashboard.
- When you want to project game status on big screen.

## Features

- [x] SandTimer                    
- [x] Counter                      
- [x] Dice                         
- [ ] Cards to draw
- [x] Full-screen responsive layout
- [x] Layout loading from Json
- [ ] Layout export
- [ ] Internationalization

